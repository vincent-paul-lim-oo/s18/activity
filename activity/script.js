console.log('S18 Activity');

function Pokemon(name, level) {
    //properties
    this.name = name;
    this.level = level;
    this.health = 3 * level;
    this.attack = level;
    //methods
    this.tackle = function (target) {
        target.health -= this.attack;
        console.log(this.name + ` tackled ` + target.name);
        console.log(target.name + `'s health is now reduced to ` + (target.health));

        if(target.health <= 0) {
			target.faint()
		}
    },
        this.faint = function () {
            console.log(this.name + ` fainted.`);
        };
        
}

let pikachu = new Pokemon(`Pikachu`, 16);
let squirtle = new Pokemon(`Squirtle`, 8);      