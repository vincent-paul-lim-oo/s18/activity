//console.log('Hello World');

//OBJECTS
    /*
        - An object is a data type that is used to represent real world objects.
        - It is also a collection of related data and/or functionalities.

        Syntax: 
            let objectName = {
                keyA: valueA,
                keyB: valueB
            }
    */
   
// Create an object

let cellphone = {
    name: 'Nokia 3210',
    manufactureDate: 1999
}

console.log(cellphone);
console.log(typeof cellphone);

//Creating objects using constructor function
/* 
    - Constructors are like blueprints or templates to create our objects form

    Syntax: 
        function ObjectName(keyA, keyB) {
            this.keyA = keyA;
            this.keyB = keyB
        }
    -The "this" keyword allows to assign a new object's properties by associating them with values received from a constructor function's parameter.
    -The "new" operator creates an instance of an object

*/

function Laptop(name, manufactureDate) {
    this.name = name;
    this.manufactureDate = manufactureDate;
}

let laptop = new Laptop('Lenovo', 2008);
console.log(laptop);

let myLaptop = new Laptop('MacBook Air', 2020);
console.log(myLaptop);

function Student(name, age, section, teacher) {
    this.name = name;
    this.age = age;
    this.section = section;
    this.teacher = teacher;

    //return name + age + section + teacher
}

let myStudentDetails = new Student('JM Quilario', 18, 'Batch 157', 'Ms. Miah');
console.log(myStudentDetails);

let oldLaptop = Laptop('Portal R2E CCMC', 1980);
console.log(oldLaptop);

//Creating empty objects
let computer = {};
let myComputer = new Object();

console.log(computer);
console.log(myComputer);

// Accessing Object Properties

//Using the dot notation
console.log(myLaptop.name);

//Using the square bracket notation
console.log(myLaptop['name']);

//Accessing array objects
let array = [laptop, myLaptop];

console.log(array[0]['name']);
console.log(array[1].name);

//Initializing/Adding/Deleting/Reassigning Object Properties
// objectName.propertyName = value

let car = {};
console.log(car);
car.name = 'Honda Civic';
console.log(car);

//Initializing/adding object properties using bracket notation
car['manufacture date'] = 2019;
console.log(car);
console.log(car['manufacture date']);

//Deleting object property
delete car['manufacture date'];
console.log(car);

car.color = 'Blue';
console.log(car)

//Reassigning Object properties
car.name = 'Ferrari';
console.log(car);

//Object Methods
    /*
        A method is also a function which is a property of an object.
    */

let person = {
    name: 'John Wick',
    talk: function () {
        console.log('Hello! My name is ' + this.name);
    } 
}
console.log(person);
person.talk();

person.walk = function() {
    console.log(this.name + ' walked 25 steps forward');
}
person.walk();
console.log(person);

//Methods are useful for creating reusable function that perform tasks related to objects

let friend = {
    firstName: 'Andrei',
    lastName: 'Limuco',
    address: {
        city: 'Austin',
        country: 'Texas',
    },
    emails: ['al@mail.com', 'andrei@mail.com'],
    introduce: function () {
        console.log('Hello! My name is '+ this.firstName + ' ' + this.lastName)
    }
}

friend.introduce()

//Real World Application of Objects
/* 
    -Scenario:
        1. We would like to create a new game that would have several pokemon interact with each other.
        2. Every pokemon should have the same set of properties and functions
*/

let myPokemon = {
    name: 'Pikachu',
    level: 3,
    health: 100,
    attack: 50,
    tackle: function() {
        console.log(`This pokemon tackled targetPokemon`)
    },

    faint: function() {
        console.log(`Pokemon fainted`);
    }
}

console.log(myPokemon);

//We will use object constructor

function Pokemon(name, level) {
    //properties
    this.name = name;
    this.level = level;
    this.health = 3 * level;
    this.attack = level;
    //methods
    this.tackle = function (target) {
        console.log(this.name + ` tackled ` + target.name);
        console.log(target.name + `'s health is now reduced to ` + (target.health - this.attack));
    },
        this.faint = function () {
        console.log(this.name + ` fainted.`);
    }
}

//Creates a new instance of the "Pokemon" object each with their unique properties
let pikachu = new Pokemon(`Pikachu`, 16);
let squirtle = new Pokemon(`Squirtle`, 8);

pikachu.tackle(squirtle)

//24 - 16 = 8